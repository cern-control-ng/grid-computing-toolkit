import requests
import os

server_url = os.environ.get('SERVER_API')
user = os.environ.get('RDP_USER_NAME')
pw = os.environ.get('RDP_USER_PW')

def request_access(url, username=None, password=None):
    try:
        if username and password:
            # If username and password are provided, include them in the request
            response = requests.get(url, auth=(username, password))
        else:
            # If username and password are not provided, make a regular request
            response = requests.get(url)
        
        if response.status_code == 200:
            return response.text
        else:
            print("Failed to retrieve content from API. Status code:", response.status_code)
            return None
    except requests.RequestException as e:
        print("Error occurred:", e)
        return None



